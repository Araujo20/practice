*** Settings ***

Resource    ${EXECDIR}/Resource/commons/Base.robot

*** Variables ***
${MENU_CAPTURA_PROPOSTA}     xpath=//*[@id="menu_proposal"]    
${SUB_MENU_PRE_AVALIACAO}    link=Pré avaliação


*** Keywords ***
Dado que o usuario acesse o menu sistema
    Sleep                          2
    Page Should Contain Element    //*[@id="credzLogoHeaderDesktop"]    
    ##Verificando se há a imagem da credz
    Sleep                          2
    Capturar tela

Quando selecionar o sub-menu pre avaliacao
    Mouse Over                       ${MENU_CAPTURA_PROPOSTA} 
    Wait Until Element Is Visible    ${SUB_MENU_PRE_AVALIACAO}    20
    Click Link                       ${SUB_MENU_PRE_AVALIACAO}
    Page Should Contain              Pré Avaliação


E preencher os dados da pre avaliacao
    Sleep           3
    ${CPF}=         Ler dado da planilha         cpf         A3
    Press Keys      id=Cpf                       ${CPF}
    Sleep           1
    Press Keys      xpath=//*[@id="Birthday"]    10101990
    Click Button    id=btnConsult                

E preencher os dados
    Wait Until Element Is Visible    //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]    60
    Set Focus To Element             //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]
    Click Element                    //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]
    Wait Until Element Is Visible    id=FullName                                                                             20
    Input Text                       id=FullName                                                                             Fulano Da Silva
    Select From List By Value        xpath=//*[@id="Gender"]                                                                 M
    Wait Until Element Is Visible    id=IdCivilStatus
    Select From List By Value        id=IdCivilStatus                                                                        1
    Select From List By Value        id=Nationality                                                                          BRA
    Mouse Over                       xpath=//*[@id="MotherName"]
    Input Text                       id=MotherName                                                                           Matilda Da Silva
    Select From List By Value        id=IdPersonalTypeDocument                                                               1
    Input Text                       id=DocumentNumber                                                                       3053000
    Sleep                            1
    Capturar tela
    Select From List By Value        id=IdBornPlace                                                                          223                 
    ##Selecionando DF
    Sleep                            1
    Select From List By Value        //*[@id="IdDispatcherRegion"]                                                           223
    Press Keys                       id=DispatcherDate                                                                       10102000
    Sleep                            1
    Press Key                        //*[@id="DDDCelPhone"]                                                                  61
    Press Keys                       id=ZipCode                                                                              72310-403
    Sleep                            1
    Input Text                       id=Number                                                                               31
    Press Keys                       id=Number                                                                               TAB                 
    Input Text                       id=Number                                                                               31
    ##Nesse campos foi necessário utilizar a repeat kw devido à regra de alguns campos especídicos
    Sleep                            1
    Capturar tela
    Set Focus To Element             //*[@id="DDDCelPhone"]
    Press Key                        //*[@id="DDDCelPhone"]                                                                  61
    Sleep                            1
    Press Key                        //*[@id="DDDCelPhone"]                                                                  61                  
    ##a repetição abaixo faz com que o campo telefone fique preenchido
    Input Text                       //*[@id="CelPhone"]                                                                     993939393           
    Clear Element Text               //*[@id="CelPhone"]
    Press Keys                       //*[@id="CelPhone"]                                                                     993939393
    Press Keys                       //*[@id="CelPhone"]                                                                     TAB                 
    Input Text                       //*[@id="CelPhone"]                                                                     993939393
    Sleep                            1
    Press Keys                       //*[@id="CelPhone"]                                                                     TAB
    Press Keys                       //*[@id="CelPhone"]                                                                     993939393
    Press Keys                       id=Email                                                                                TAB
    Press Keys                       id=Email                                                                                teste@teste.com
    Press Keys                       id=Email                                                                                TAB
    Press Keys                       id=Email                                                                                teste@teste.com
    Sleep                            2
    Select From List By Value        id=ContactOption                                                                        EMAIL
    Select From List By Value        id=MailAddress                                                                          R
    Capturar tela
    ##Dado profissionais
    Select From List By Value        id=IdRootOccupation                                                                     2
    ##Selecionando advogado pelo value 2571
    Sleep                            2
    Select From List By Value        id=IdOccupation                                                                         2571
    Input Text                       id=MonthyRent                                                                           100000
    ##Secao produtos desejados
    Wait Until Element Is Visible    id=PayDay                                                                               
    Select From List By Value        id=PayDay                                                                               20
    ##Selecionando cartão
    Select From List By Index        id=IdDocumentType                                                                       1
    #Select From List By Index        id=IdBornPlace                                                                          223
    Press Key                        xpath=//*[@id="birthCityDescription"]                                                   BRASILIA
    Press Keys                       xpath=//*[@id="birthCityDescription"]                                                   DOWN                
    Sleep                            2
    Press Keys                       xpath=//*[@id="birthCityDescription"]                                                   TAB

E selecionar a opção imprimir proposta
    Wait Until Element Is Visible    id=btoPrint
    Click Element                    id=btoPrint
    Wait Until Element Is Visible    //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]/span
    Click Element                    //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]/span
    Sleep                            8                                                                                            
    Click Element                    //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]/span

Então o sistema apresenta a mensagem de confirmação de envio
    Capturar tela
    Click Button                     id=btnSave                                                                                   
    Wait Until Element Is Visible    //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]/span    120
    Click Element                    //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]/span
    Capturar tela
    Sleep                            2                                                                                            
    Click Element                    //*[contains(@class, 'ui-dialog')][contains(@style,'block')][1]/div[3]/div/button[1]/span 



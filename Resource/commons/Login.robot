*** Settings ***
Resource    ${EXECDIR}/Resource/commons/Base.robot    
Library     ${EXECDIR}/Utils/LerPlanilha.py 

*** Variables ***
${USUARIO}        686.634.521-95
${SENHA}          aaa123
${BTN_ENVIAR}     name=submit_search
${CAMPO_CPF}      id=cpf 
${CAMPO_SENHA}    id=password
${CAMPO_PASS}     id=fakepassword

*** Keywords ***
Realizar login
    Press Key                       ${CAMPO_CPF}                        ${USUARIO}
    ##Foi utilizada a press key, devido à másccara do campo
    Set Focus To Element            id=fakepassword
    Input Text                      id=password                         ${SENHA}
    Capturar tela
    Click Element                   xpath=//*[@id="form"]/p[5]/input
    Sleep         5
    Capturar tela
    Select From List By Value       id=Company                          24            
    Click Button                    id=btnSelect
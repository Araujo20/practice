*** Settings ***

Resource    ${EXECDIR}/Resource/Commons/Base.robot 
Resource    ${EXECDIR}/Resource/Commons/Login.robot    
Resource    ${EXECDIR}/Resource/PageObjects/PropostaUsuarioPage.robot   

Suite Setup     Abrir navegador
Test Setup      Realizar login       
#Test Teardown   Fechar navegador

*** Variables ***

*** Test Cases ***
Cenário 001: Enviar proposta
    Dado que o usuario acesse o menu sistema 
    Quando selecionar o sub-menu pre avaliacao
    E preencher os dados da pre avaliacao
    E preencher os dados
    E selecionar a opção imprimir proposta
    Então o sistema apresenta a mensagem de confirmação de envio

    

*** Settings ***
Library         SeleniumLibrary


*** Variables ***
${URL}                      http://projeto3.credz.cwi.com.br/SGC/  
${NAVEGADOR}                chrome       


*** Keywords ***
Abrir navegador
    Open Browser                         ${URL}        ${NAVEGADOR}
    Sleep       2
    Maximize Browser Window
           

Capturar tela
    Capture Page Screenshot

Ler dado da planilha
    [Arguments]     ${Sheetname}        ${cell}  
    ${cell_data}=   ler_dado_celula     ${Sheetname}    ${cell}    
    [Return]        ${cell_data}

Fechar navegador
    Close Browser            